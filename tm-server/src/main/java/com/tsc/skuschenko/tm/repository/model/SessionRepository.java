package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.ISessionRepository;
import com.tsc.skuschenko.tm.model.Session;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull @Autowired EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public @Nullable List<Session> findAll(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM SessionDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, Session.class)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    public Session findSessionById(Session session) {
        return entityManager.find(Session.class, session.getId());
    }

    @Nullable
    @Override
    public Session getReference(@NotNull final String id) {
        return entityManager.getReference(Session.class, id);
    }

    @Override
    public void removeSessionById(Session session) {
        @NotNull final String query =
                "DELETE FROM SessionDTO e WHERE e.id = :id";
        entityManager.createQuery(query)
                .setParameter("id", session.getId())
                .executeUpdate();
    }

}
