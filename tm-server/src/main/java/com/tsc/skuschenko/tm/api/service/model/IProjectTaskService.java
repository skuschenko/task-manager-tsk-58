package com.tsc.skuschenko.tm.api.service.model;

import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskByProject(
            @NotNull String userId, @Nullable Project project,
            @Nullable String taskId);

    void clearProjects(@NotNull String userId);

    @Nullable
    Project deleteProjectById(
            @NotNull String userId, @Nullable String projectId
    );

    @Nullable
    List<Task> findAllTaskByProjectId(
            @NotNull String userId, @Nullable String projectId
    );

    @NotNull
    Task unbindTaskFromProject(
            @NotNull String userId, @Nullable String projectId,
            @Nullable String taskId
    );

}
