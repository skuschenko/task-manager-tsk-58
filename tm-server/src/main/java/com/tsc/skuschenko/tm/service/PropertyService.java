package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Nullable
    @Value("${version}")
    private String applicationVersion;

    @Nullable
    @Value("${author}")
    private String author;

    @Nullable
    @Value("${author.email}")
    private String authorEmail;

    @Nullable
    @Value("${backupTime:60}")
    private Integer backupTime;

    @Nullable
    @Value("${jdbc.cache.factoryClass}")
    private String factoryClass;

    @Nullable
    @Value("${filePath.backupXml}")
    private String fileBackupPath;

    @Nullable
    @Value("${filePath.base64}")
    private String fileBase64Path;

    @Nullable
    @Value("${filePath.binary}")
    private String fileBinaryPath;

    @Nullable
    @Value("${filePath.jaxbJson}")
    private String filePathJaxbJson;

    @Nullable
    @Value("${filePath.jaxbXml}")
    private String filePathJaxbXml;

    @Nullable
    @Value("${filePath.json}")
    private String filePathJson;

    @Nullable
    @Value("${filePath.xml}")
    private String filePathXml;

    @Nullable
    @Value("${fileScannerTime:60}")
    private Integer fileScannerTime;

    @Nullable
    @Value("${filePath.yaml}")
    private String fileYamlPath;

    @Nullable
    @Value("${jdbc.dialect}")
    private String jdbcDialect;

    @NotNull
    @Value("${jdbc.driver}")
    private String jdbcDriver;

    @Nullable
    @Value("${jdbc.hbm2ddl}")
    private String jdbcHbm2ddl;

    @Nullable
    @Value("${jdbc.password}")
    private String jdbcPassword;

    @Nullable
    @Value("${jdbc.showSql}")
    private String jdbcShowSql;

    @Nullable
    @Value("${jdbc.url}")
    private String jdbcUrl;

    @Nullable
    @Value("${jdbc.userName}")
    private String jdbcUserName;

    @Nullable
    @Value("${jdbc.cache.liteMember}")
    private String liteMember;

    @Nullable
    @Value("${jdbc.cache.minimalPuts}")
    private String minimalPuts;

    @Nullable
    @Value("${password.iteration:25456}")
    private Integer passwordIteration;

    @Nullable
    @Value("${password.secret:356585985}")
    private String passwordSecret;

    @Nullable
    @Value("${jdbc.cache.providerConfiguration}")
    private String providerConfiguration;

    @Nullable
    @Value("${jdbc.cache.queryCache}")
    private String queryCache;

    @Nullable
    @Value("${jdbc.cache.regionPrefix}")
    private String regionPrefix;

    @Nullable
    @Value("${jdbc.cache.secondLevel}")
    private String secondLevel;

    @Nullable
    @Value("${server.host}")
    private String serverHost;

    @Nullable
    @Value("${server.port}")
    private String serverPort;

    @Nullable
    @Value("${session.cycle}")
    private String sessionCycle;

    @Nullable
    @Value("${session.salt}")
    private String sessionSalt;

    @Nullable
    @Override
    public String getFileJsonPath(@NotNull final String className) {
        switch (className) {
            case "jaxB":
                return filePathJaxbJson;
            default:
                return filePathJson;
        }
    }

    @Nullable
    @Override
    public String getFileXmlPath(@NotNull final String className) {
        switch (className) {
            case "jaxb":
                return filePathJaxbXml;
            default:
                return filePathXml;
        }
    }

}