package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IProjectDTORepository;
import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.api.service.dto.IProjectTaskDTOService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.repository.dto.ProjectDTORepository;
import com.tsc.skuschenko.tm.repository.dto.TaskDTORepository;
import com.tsc.skuschenko.tm.service.AbstractService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public final class ProjectTaskDTOService extends AbstractService
        implements IProjectTaskDTOService {

    @NotNull
    private final String PROJECT_ID = "project id";

    @NotNull
    private final String TASK_ID = "task id";

    @NotNull
    @Override
    public TaskDTO bindTaskByProject(
            @NotNull final String userId, @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectDTORepository projectRepository =
                context.getBean(ProjectDTORepository.class, entityManager);
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        @NotNull final ITaskDTORepository taskRepository =
                context.getBean(TaskDTORepository.class, entityManager);
        @NotNull final TaskDTO task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectDTORepository projectRepository =
                context.getBean(ProjectDTORepository.class, entityManager);
        @Nullable final List<ProjectDTO> projects =
                projectRepository.findAllWithUserId(userId);
        @Nullable final Optional<List<ProjectDTO>> findProjects =
                Optional.ofNullable(projects).filter(item -> item.size() != 0);
        findProjects.ifPresent(items -> items.forEach(item ->
                deleteProjectById(userId, item.getId())));
    }

    @Nullable
    @Override
    public ProjectDTO deleteProjectById(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        @Nullable final List<TaskDTO> projectTasks =
                findAllTaskByProjectId(userId, projectId);
        @Nullable final Optional<List<TaskDTO>> tasks =
                Optional.ofNullable(projectTasks)
                        .filter(item -> item.size() != 0);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectDTORepository projectRepository =
                context.getBean(ProjectDTORepository.class, entityManager);
        @NotNull final ITaskDTORepository taskRepository =
                context.getBean(TaskDTORepository.class, entityManager);
        try {
            entityManager.getTransaction().begin();
            tasks.ifPresent(items -> items.forEach(taskRepository::remove));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project =
                    projectRepository.findOneById(userId, projectId);
            projectRepository.removeOneById(userId, projectId);
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllTaskByProjectId(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final ITaskDTORepository taskRepository =
                context.getBean(TaskDTORepository.class, entityManager);
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId, @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectDTORepository projectRepository =
                context.getBean(ProjectDTORepository.class, entityManager);
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        @NotNull final ITaskDTORepository taskRepository =
                context.getBean(TaskDTORepository.class, entityManager);
        @NotNull final TaskDTO task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        try {
            entityManager.getTransaction().begin();
            task.setProjectId(null);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
