package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.IProjectDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.configuration.ServerConfiguration;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.repository.dto.ProjectDTORepository;
import com.tsc.skuschenko.tm.service.PropertyService;
import org.hibernate.UnresolvableObjectException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.EntityManager;

public class ProjectRepositoryTest {

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @NotNull
    private static  EntityManager entityManager;

    @BeforeClass
    public static void before() {
        context =
                new AnnotationConfigApplicationContext(
                        ServerConfiguration.class
                );
        entityManager = context.getBean(EntityManager.class);
    }

    @AfterClass
    public static void after() {
        entityManager.close();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus("status1");
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals("status1", projectFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus("status1");
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals("status1", projectFind.getStatus());
    }

    @Test(expected = UnresolvableObjectException.class)
    public void testClear() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        entityManager.getTransaction().begin();
        projectRepository.clearAllProjects();
        entityManager.getTransaction().commit();
        entityManager.refresh(project);
    }

    @Test
    public void testCompleteById() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        project.setStatus(Status.COMPLETE.getDisplayName());
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getName());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final ProjectDTO project = testProjectModel();
        testRepository(project);
    }

    @Test
    public void testFindOneById() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind);
    }

    @Test
    public void testFindOneByName() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind);
    }

    @NotNull
    private ProjectDTO testProjectModel() {
        @Nullable final ProjectDTO project = new ProjectDTO();
        project.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        project.setName("name1");
        project.setDescription("des1");
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("name1", project.getName());
        return project;
    }

    @Test(expected = UnresolvableObjectException.class)
    public void testRemoveOneById() {
        @Nullable final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        entityManager.getTransaction().begin();
        projectRepository.removeOneById(project.getUserId(), project.getId());
        entityManager.getTransaction().commit();
        entityManager.refresh(project);
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNull(projectFind);
    }

    @NotNull
    private IProjectDTORepository testRepository(
            @NotNull final ProjectDTO project
    ) {
        @NotNull final IProjectDTORepository projectRepository =
               context.getBean(ProjectDTORepository.class,entityManager);
        System.out.println(entityManager.hashCode());
        entityManager.getTransaction().begin();
        projectRepository.clearAllProjects();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        projectRepository.add(project);
        entityManager.getTransaction().commit();
        @Nullable final ProjectDTO projectById =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        return projectRepository;
    }

    @Test
    public void testStartById() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getStatus());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByName(
                        project.getUserId(), project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), projectFind.getStatus()
        );
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setName("name2");
        project.setDescription("des2");
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findById(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

    @Test
    public void testUpdateOneByIndex() {
        @NotNull final ProjectDTO project = testProjectModel();
        @NotNull final IProjectDTORepository
                projectRepository = testRepository(project);
        project.setName("name2");
        project.setDescription("des2");
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final ProjectDTO projectFind =
                projectRepository.findOneByIndex(project.getUserId(), 0);
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

}
