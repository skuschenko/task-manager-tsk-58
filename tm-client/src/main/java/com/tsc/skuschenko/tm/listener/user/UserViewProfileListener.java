package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    private static final String DESCRIPTION = "view profile of current user";

    private static final String NAME = "view-user-profile";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @Nullable final User user =
                Optional.ofNullable(userEndpoint.getUser())
                        .orElseThrow(UserNotFoundException::new);
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Override
    public String name() {
        return NAME;
    }

}
