package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService{

    @NotNull
    String getApplicationVersion();

}
